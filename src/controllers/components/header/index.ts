import {useState} from "react";
import {useNavigate} from "react-router-dom";

export const useHeader = () => {
    const [text,setText] = useState<string>('')
    const navigate = useNavigate()
    const navigateToSearch = (goToHome= false) => {
        goToHome ? navigate('/') :navigate('/items?search='+text)
    }
   return {
       text,
       setText,
       navigateToSearch
   }
 }