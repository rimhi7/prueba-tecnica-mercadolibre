import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {useProductActions} from "../../../api";
import {useEffect, useRef} from "react";
import {useNavigate, useSearchParams} from "react-router-dom";

export const useHomeController = () => {
    const {data} = useAppSelector((state)=>state.products)
    const {getProductsAction, searchProductAction} = useProductActions();
    const dispatch:any = useAppDispatch()
    const navigate = useNavigate()
    const hasMount = useRef(false)
    const [params] = useSearchParams();
    const search = params.get('search')
    useEffect(()=>{
        if(hasMount.current){
            if(window.location.pathname === '/items' && search){
                dispatch(searchProductAction(search))
            }else{
                dispatch(getProductsAction())
            }
        }
        return ()=>{
            hasMount.current = true
        }
    },[search])
    const goToDetail = (id:string) => {
        navigate('/items/'+id)
    }
    return {
        data,
        goToDetail
    }
}