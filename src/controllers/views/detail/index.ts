import {useEffect, useRef} from "react";
import {useProductActions} from "../../../api";
import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {useParams} from "react-router-dom";

export const useDetailController = () => {
  const {getProductAction,getProductDescriptionAction} = useProductActions()
  const dispatch:any = useAppDispatch()
  const {id} = useParams()
    const {item} = useAppSelector((state)=>state.products)
  const hasMount = useRef(false)

  useEffect(()=>{

    if(hasMount.current){
      dispatch(getProductAction(`${id}`))
      dispatch(getProductDescriptionAction(`${id}`))
    }
   return ()=>{
      hasMount.current = true
   }
  },[id])
  return {
    item
  }
}