import React from 'react';

import './App.css';
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import {Detail, Home} from "./views";
import {useInterceptor} from "./config";
import {persist, store} from "./config/redux";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Home/>,
    },
    {
        path: "/items/:id",
        element: <Detail/>,
    },
    {
        path: "/items",
        element: <Home/>,
    }
]);

function App() {

  useInterceptor(store);
  return (
      <div>
        <Provider store={store}>
          <PersistGate persistor={persist}>
            <RouterProvider router={router} />
          </PersistGate>
        </Provider>

      </div>

  );
}

export default App;
