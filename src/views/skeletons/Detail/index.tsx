import '../../../assets/styles/detailskeleton.css'
export const DetailSkeleton = () => {
    return (
        <>
            <div className={'breadcrumb-skeleton'}></div>
    <div className={'detail-container-skeleton'}>
    <div className={'detail-image-skeleton'}>
    </div>
    <div className={'detail-price-skeleton'}>
    <p className={'category-skeleton'}>
    </p>
    <p className={'title-skeleton'}></p>
        <p className={'price-skeleton'}>
        </p>
        <button className={'btn-buy-skeleton'}></button>
        </div>
        <div className={'detail-description-skeleton'}>
    <span className={'title-description-skeleton'}>Descripcion del Producto</span>
    <p className={'description-skeleton'}></p>
        </div>
        </div>
        </>
)
}