
import '../../../assets/styles/cardSkeleton.css'
const CardSkeleton = () => {
  return (
      <div className={'card-skeleton'}>
          <div className={'img-card-skeleton'}>
          </div>
          <div className={'card-body-skeleton'}>
              <h1 className={'card-title-skeleton'}></h1>
              <p className={'card-description-skeleton'}>

              </p>
          </div>

          <span className={'city-card-skeleton'}></span>

      </div>
  )
}
export const CardsSkeleton = ()=>{
    return (
        <>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        <CardSkeleton/>
        </>
    )
}