import {MainLayout} from "../../Layout";
import {useDetailController} from "../../../controllers";
import {Item} from "../../../models/interfaces/products";
import {useFormatNumber} from "../../../helpers";
import {usePromiseTracker} from "react-promise-tracker";
import {DetailSkeleton} from "../../skeletons/Detail";

export const Detail = () => {
    const {item= {} as Item}= useDetailController()
    const {formatter} = useFormatNumber();
    const {promiseInProgress} = usePromiseTracker()
    return (
        <MainLayout>
            {promiseInProgress ? <DetailSkeleton/> :
                <>
                    <div className={'breadcrumb'}>
                        {item.attributes?.length>0 && (<><span className={'breadcrumb'}>{`${item.attributes[0].value_name} > `}</span>
                            <span className={'breadcrumb'}>{`${item.attributes[1].value_name} > `}</span>
                            <span className={'breadcrumb'}>{`${item.attributes[2].value_name} `}</span></>)}

                    </div>
                    <div className={'detail-container'}>
                        <div className={'detail-image'}>
                            <img
                                alt={item.title}
                                src={item.thumbnail}
                                className={'image'}
                                height={438}
                                width={438}
                            />

                        </div>
                        <div className={'detail-price'}>
                            <p className={'category'}>
                                {item.condition} - {item.sold_quantity} vendidos
                            </p>
                            <p className={'title'}>
                                {item.title}
                            </p>
                            <p className={'price'}>
                                <span>{formatter.format(item.price)}</span>
                            </p>
                            <button className={'btn-buy'}>Comprar</button>
                        </div>
                        <div className={'detail-description'}>
                            <span className={'title-description'}>Descripcion del Producto</span>
                            <p className={'description'}>
                                {item.plain_text}
                            </p>
                        </div>
                    </div>
                </>
            }

        </MainLayout>
    )
}