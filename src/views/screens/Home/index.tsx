import {MainLayout} from "../../Layout";
import {Card} from "../../components";
import {useHomeController} from "../../../controllers";
import {Suspense} from "react";
import {CardsSkeleton} from "../../skeletons";
import {usePromiseTracker} from "react-promise-tracker";

export const Home = () => {
const {data,goToDetail} = useHomeController()
   const {promiseInProgress} = usePromiseTracker()
    return (
        <MainLayout>
            {promiseInProgress ? <CardsSkeleton/>:
            <div data-cy="container-cards">

                <Suspense fallback={
                    <CardsSkeleton/>
                }>
                <>
                {data.map((item)=>
                    <Card
                        item={item}
                        key={item.id}
                        onClick={goToDetail}
                    />
                )}
                    </>
                </Suspense>
            </div>
            }
        </MainLayout>
    )
}