import {ReactNode} from "react";
import {Header} from "../../components";

type Props = {
    children:ReactNode
}
export const MainLayout = ({children}:Props) => {
  return(
      <div>
          <Header/>
          <section className={'main-section'}>
              {children}
          </section>
      </div>
  )
}