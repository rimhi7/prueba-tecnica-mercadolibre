import {Item} from "../../../models/interfaces/products";
import {useFormatNumber} from "../../../helpers";

type Props = {
    item:Item
    onClick:(id:string)=>void
}
const Card = ({item, onClick}:Props) => {
    const {formatter} = useFormatNumber();
    return (

        <div className={'card'} onClick={()=>onClick(item.id)}>
            <div className={'img-card'}>
                <img
                    src={item.thumbnail}
                    width={160} height={160}
                    alt={item.title}
                />
            </div>
            <div className={'card-body'}>
                <h1 className={'card-title'}>{formatter.format(item.price)}</h1>
                <p className={'card-description'}>
                    {item.title}
                </p>
            </div>

            <span className={'city-card'}>{item.address.state_name}</span>

        </div>
    )
}
export default Card