import {AiOutlineSearch} from "react-icons/ai";
import {useHeader} from "../../../controllers";


export const Header = ()=>{
    const {text, setText,navigateToSearch} =  useHeader();
    return(
        <nav className={'navigation'}>
            <div data-cy="icon-button" className={'icon'} onClick={()=>navigateToSearch(true)}>
               <img
                   alt={'logo'}
                   width={157}
                   height={40}
                   src={'https://http2.mlstatic.com/frontend-assets/ml-web-navigation/ui-navigation/5.22.8/mercadolibre/logo__large_plus@2x.png'}
               />
            </div>
                <div className={'container-search'}>
                    <input
                        className={'search'}
                        placeholder={'Nunca dejes de buscar'}
                        value={text}
                        onKeyPress={(e)=>
                            e.key === 'Enter' && navigateToSearch()
                        }
                        data-cy="input-search"
                        onChange={(e)=>setText(e.target.value)}
                    />
                    <button  data-cy="button-search" id={'search'} aria-label={'Search'} className={'button-search'} onClick={()=>navigateToSearch()} >
                        <AiOutlineSearch size={20.5}/>
                    </button>
                </div>
        </nav>
    )
}