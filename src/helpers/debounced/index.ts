import {useEffect, useState} from "react";

type Props = {
    value:string
    time?:number
}
export const useDebounced = ({ value, time = 500 }:Props)=>{
    const [text,setText] = useState(value)
    useEffect(()=>{
        const timeout = setTimeout(
            ()=>{
                setText(value)
            },time)
        return ()=>{
            clearTimeout(timeout)
        }

    },[value,time])
    return text
}