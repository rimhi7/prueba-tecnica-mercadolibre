import axios, {AxiosError, AxiosResponse, InternalAxiosRequestConfig} from 'axios';
import {useEffect} from "react";
import {EnhancedStore} from "@reduxjs/toolkit";

export const useInterceptor = (store:EnhancedStore) => {
    const handleRequestSuccess = (request: InternalAxiosRequestConfig<Response>)=>{
        return request
    }
    const handleRequestError = (error:AxiosError)=>{
        console.error('REQUEST ERROR',error)
    }
    const  handleResponseSuccess = (response:AxiosResponse)=>{
        return response
    }
    const handleResponseError = (error:AxiosError)=>{
        return error
    }

    useEffect(()=>{
        axios.defaults.baseURL = `https://api.mercadolibre.com`
        axios.interceptors.request.use(handleRequestSuccess,handleRequestError)
        axios.interceptors.response.use(handleResponseSuccess,handleResponseError)
    },[])
}