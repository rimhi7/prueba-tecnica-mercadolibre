import {Item} from "../interfaces/products";

type ProductState = {
    item?:Item
    data:Item[]
}
export const handleInitialStates = () => {
    const products:ProductState  = {
        data:[],

    }
  return {
      products
  }
}