export interface ResponseItems {
    results:Item[]
}
export interface Sort {
    id:   null | string;
    name: string;
}

export interface Paging {
    total:           number;
    primary_results: number;
    offset:          number;
    limit:           number;
}

export interface Item {
    id:                    string;
    title:                 string;
    plain_text?:            string;
    condition:             Condition;
    thumbnail_id:          string;
    catalog_product_id:    null | string;
    listing_type_id:       ListingTypeID;
    permalink:             string;
    buying_mode:           BuyingMode;
    site_id:               SiteID;
    category_id:           CategoryID;
    domain_id:             DomainID;
    thumbnail:             string;
    currency_id:           CurrencyID;
    order_backend:         number;
    price:                 number;
    original_price:        number | null;
    sale_price:            null;
    sold_quantity:         number;
    available_quantity:    number;
    official_store_id:     number | null;
    use_thumbnail_id:      boolean;
    accepts_mercadopago:   boolean;
    tags:                  ResultTag[];
    shipping:              Shipping;
    stop_time:             Date;
    seller:                Seller;
    seller_address:        SellerAddress;
    address:               Address;
    attributes:            Attribute[];
    installments:          Installments;
    winner_item_id:        null;
    discounts:             null;
    promotions:            any[];
    differential_pricing?: DifferentialPricing;
    inventory_id:          null | string;
    catalog_listing?:      boolean;
    variation_filters?:    string[];
    variations_data?:      { [key: string]: VariationsDatum };
    official_store_name?:  string;
}

export interface Address {
    state_id:   StateID;
    state_name: StateName;
    city_id:    null | string;
    city_name:  string;
}

export enum StateID {
    ArB = "AR-B",
    ArC = "AR-C",
    ArT = "AR-T",
    ArX = "AR-X",
}

export enum StateName {
    BuenosAires = "Buenos Aires",
    CapitalFederal = "Capital Federal",
    Córdoba = "Córdoba",
    Tucumán = "Tucumán",
}

export interface Attribute {
    id:                   string;
    name:                 string;
    value_id:             null | string;
    value_name:           string;
    attribute_group_id:   AttributeGroupID;
    attribute_group_name: AttributeGroupName;
    value_struct:         Struct | null;
    values:               AttributeValue[];
    source:               number;
    value_type:           ValueType;
}

export enum AttributeGroupID {
    Others = "OTHERS",
}

export enum AttributeGroupName {
    Otros = "Otros",
}

export interface Struct {
    number: number;
    unit:   Unit;
}

export enum Unit {
    CM = "cm",
    G = "g",
    Kg = "kg",
    M = "m",
    Mm = "mm",
}

export enum ValueType {
    List = "list",
    Number = "number",
    NumberUnit = "number_unit",
    String = "string",
}

export interface AttributeValue {
    id:     null | string;
    name:   string;
    struct: Struct | null;
    source: number;
}

export enum BuyingMode {
    BuyItNow = "buy_it_now",
}

export enum CategoryID {
    Mla10089 = "MLA10089",
    Mla1649 = "MLA1649",
    Mla1672 = "MLA1672",
    Mla1694 = "MLA1694",
    Mla352001 = "MLA352001",
    Mla432485 = "MLA432485",
    Mla72894 = "MLA72894",
}

export enum Condition {
    New = "new",
}

export enum CurrencyID {
    Ars = "ARS",
}

export interface DifferentialPricing {
    id: number;
}

export enum DomainID {
    MlaDesktopComputers = "MLA-DESKTOP_COMPUTERS",
    MlaGamepadsAndJoysticks = "MLA-GAMEPADS_AND_JOYSTICKS",
    MlaGraphicsTablets = "MLA-GRAPHICS_TABLETS",
    MlaHardDriveAndSSDCaddies = "MLA-HARD_DRIVE_AND_SSD_CADDIES",
    MlaHardDrivesAndSsds = "MLA-HARD_DRIVES_AND_SSDS",
    MlaRAMMemoryModules = "MLA-RAM_MEMORY_MODULES",
    MlaStreamingMediaDevices = "MLA-STREAMING_MEDIA_DEVICES",
}

export interface Installments {
    quantity:    number;
    amount:      number;
    rate:        number;
    currency_id: CurrencyID;
}

export enum ListingTypeID {
    GoldPro = "gold_pro",
    GoldSpecial = "gold_special",
}

export interface Seller {
    id:                 number;
    nickname:           string;
    car_dealer:         boolean;
    real_estate_agency: boolean;
    _:                  boolean;
    registration_date:  Date;
    tags:               SellerTag[];
    car_dealer_logo:    string;
    permalink:          string;
    seller_reputation:  SellerReputation;
    eshop?:             Eshop;
}

export interface Eshop {
    eshop_id:         number;
    seller:           number;
    nick_name:        string;
    eshop_status_id:  number;
    site_id:          SiteID;
    eshop_experience: number;
    eshop_rubro:      null;
    eshop_locations:  any[];
    eshop_logo_url:   string;
}

export enum SiteID {
    Mla = "MLA",
}

export interface SellerReputation {
    level_id:            LevelID;
    power_seller_status: PowerSellerStatus | null;
    transactions:        Transactions;
    metrics:             Metrics;
}

export enum LevelID {
    The4_LightGreen = "4_light_green",
    The5_Green = "5_green",
}

export interface Metrics {
    sales:                 Sales;
    claims:                Cancellations;
    delayed_handling_time: Cancellations;
    cancellations:         Cancellations;
}

export interface Cancellations {
    period:    CancellationsPeriod;
    rate:      number;
    value:     number;
    excluded?: Excluded;
}

export interface Excluded {
    real_value: number;
    real_rate:  number;
}

export enum CancellationsPeriod {
    The365Days = "365 days",
    The60Days = "60 days",
}

export interface Sales {
    period:    CancellationsPeriod;
    completed: number;
}

export enum PowerSellerStatus {
    Platinum = "platinum",
}

export interface Transactions {
    canceled:  number;
    completed: number;
    period:    TransactionsPeriod;
    ratings:   Ratings;
    total:     number;
}

export enum TransactionsPeriod {
    Historic = "historic",
}

export interface Ratings {
    negative: number;
    neutral:  number;
    positive: number;
}

export enum SellerTag {
    Brand = "brand",
    CreditsPriority1 = "credits_priority_1",
    CreditsPriority3 = "credits_priority_3",
    CreditsPriority4 = "credits_priority_4",
    CreditsProfile = "credits_profile",
    Developer = "developer",
    Eshop = "eshop",
    LargeSeller = "large_seller",
    MediumSeller = "medium_seller",
    MessagesAsSeller = "messages_as_seller",
    Mshops = "mshops",
    Normal = "normal",
}

export interface SellerAddress {
    comment:      string;
    address_line: string;
    id:           null;
    latitude:     null;
    longitude:    null;
    country:      Sort;
    state:        Sort;
    city:         Sort;
}

export interface Shipping {
    store_pick_up: boolean;
    free_shipping: boolean;
    logistic_type: LogisticType;
    mode:          Mode;
    tags:          ShippingTag[];
    promise:       null;
}

export enum LogisticType {
    CrossDocking = "cross_docking",
    DropOff = "drop_off",
    Fulfillment = "fulfillment",
    XdDropOff = "xd_drop_off",
}

export enum Mode {
    Me2 = "me2",
}

export enum ShippingTag {
    Fulfillment = "fulfillment",
    MandatoryFreeShipping = "mandatory_free_shipping",
    SelfServiceIn = "self_service_in",
    SelfServiceOut = "self_service_out",
}

export enum ResultTag {
    BestSellerCandidate = "best_seller_candidate",
    BrandVerified = "brand_verified",
    CartEligible = "cart_eligible",
    DealOfTheDay = "deal_of_the_day",
    DraggedBidsAndVisits = "dragged_bids_and_visits",
    ExtendedWarrantyEligible = "extended_warranty_eligible",
    GoodQualityPicture = "good_quality_picture",
    GoodQualityThumbnail = "good_quality_thumbnail",
    ImmediatePayment = "immediate_payment",
    LightningDeal = "lightning_deal",
    LoyaltyDiscountEligible = "loyalty_discount_eligible",
    MeliChoiceCandidate = "meli_choice_candidate",
    ModerationPenalty = "moderation_penalty",
    MshopsAhora12 = "mshops_ahora-12",
    MshopsAhora6 = "mshops_ahora-6",
    ShippingGuaranteed = "shipping_guaranteed",
    StandardPriceByChannel = "standard_price_by_channel",
}

export interface VariationsDatum {
    thumbnail:    string;
    ratio:        string;
    name:         string;
    pictures_qty: number;
    inventory_id: string;
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
function invalidValue(typ: any, val: any, key: any, parent: any = ''): never {
    const prettyTyp = prettyTypeName(typ);
    const parentText = parent ? ` on ${parent}` : '';
    const keyText = key ? ` for key "${key}"` : '';
    throw Error(`Invalid value${keyText}${parentText}. Expected ${prettyTyp} but got ${JSON.stringify(val)}`);
}

function prettyTypeName(typ: any): string {
    if (Array.isArray(typ)) {
        if (typ.length === 2 && typ[0] === undefined) {
            return `an optional ${prettyTypeName(typ[1])}`;
        } else {
            return `one of [${typ.map(a => { return prettyTypeName(a); }).join(", ")}]`;
        }
    } else if (typeof typ === "object" && typ.literal !== undefined) {
        return typ.literal;
    } else {
        return typeof typ;
    }
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        const map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        const map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any, key: any = '', parent: any = ''): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val, key, parent);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        const l = typs.length;
        for (let i = 0; i < l; i++) {
            const typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val, key, parent);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases.map(a => { return l(a); }), val, key, parent);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue(l("array"), val, key, parent);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue(l("Date"), val, key, parent);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue(l(ref || "object"), val, key, parent);
        }
        const result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps, key, ref);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps, key, ref);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val, key, parent);
    }
    if (typ === false) return invalidValue(typ, val, key, parent);
    let ref: any = undefined;
    while (typeof typ === "object" && typ.ref !== undefined) {
        ref = typ.ref;
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
                : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
                    : invalidValue(typ, val, key, parent);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function l(typ: any) {
    return { literal: typ };
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

function m(additional: any) {
    return { props: [], additional };
}

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Welcome": o([
        { json: "site_id", js: "site_id", typ: r("SiteID") },
        { json: "country_default_time_zone", js: "country_default_time_zone", typ: "" },
        { json: "query", js: "query", typ: "" },
        { json: "paging", js: "paging", typ: r("Paging") },
        { json: "results", js: "results", typ: a(r("Result")) },
        { json: "sort", js: "sort", typ: r("Sort") },
        { json: "available_sorts", js: "available_sorts", typ: a(r("Sort")) },
        { json: "filters", js: "filters", typ: a("any") },
        { json: "available_filters", js: "available_filters", typ: a(r("AvailableFilter")) },
    ], false),
    "AvailableFilter": o([
        { json: "id", js: "id", typ: "" },
        { json: "name", js: "name", typ: "" },
        { json: "type", js: "type", typ: "" },
        { json: "values", js: "values", typ: a(r("AvailableFilterValue")) },
    ], false),
    "AvailableFilterValue": o([
        { json: "id", js: "id", typ: "" },
        { json: "name", js: "name", typ: "" },
        { json: "results", js: "results", typ: 0 },
    ], false),
    "Sort": o([
        { json: "id", js: "id", typ: u(null, "") },
        { json: "name", js: "name", typ: "" },
    ], false),
    "Paging": o([
        { json: "total", js: "total", typ: 0 },
        { json: "primary_results", js: "primary_results", typ: 0 },
        { json: "offset", js: "offset", typ: 0 },
        { json: "limit", js: "limit", typ: 0 },
    ], false),
    "Result": o([
        { json: "id", js: "id", typ: "" },
        { json: "title", js: "title", typ: "" },
        { json: "condition", js: "condition", typ: r("Condition") },
        { json: "thumbnail_id", js: "thumbnail_id", typ: "" },
        { json: "catalog_product_id", js: "catalog_product_id", typ: u(null, "") },
        { json: "listing_type_id", js: "listing_type_id", typ: r("ListingTypeID") },
        { json: "permalink", js: "permalink", typ: "" },
        { json: "buying_mode", js: "buying_mode", typ: r("BuyingMode") },
        { json: "site_id", js: "site_id", typ: r("SiteID") },
        { json: "category_id", js: "category_id", typ: r("CategoryID") },
        { json: "domain_id", js: "domain_id", typ: r("DomainID") },
        { json: "thumbnail", js: "thumbnail", typ: "" },
        { json: "currency_id", js: "currency_id", typ: r("CurrencyID") },
        { json: "order_backend", js: "order_backend", typ: 0 },
        { json: "price", js: "price", typ: 3.14 },
        { json: "original_price", js: "original_price", typ: u(0, null) },
        { json: "sale_price", js: "sale_price", typ: null },
        { json: "sold_quantity", js: "sold_quantity", typ: 0 },
        { json: "available_quantity", js: "available_quantity", typ: 0 },
        { json: "official_store_id", js: "official_store_id", typ: u(0, null) },
        { json: "use_thumbnail_id", js: "use_thumbnail_id", typ: true },
        { json: "accepts_mercadopago", js: "accepts_mercadopago", typ: true },
        { json: "tags", js: "tags", typ: a(r("ResultTag")) },
        { json: "shipping", js: "shipping", typ: r("Shipping") },
        { json: "stop_time", js: "stop_time", typ: Date },
        { json: "seller", js: "seller", typ: r("Seller") },
        { json: "seller_address", js: "seller_address", typ: r("SellerAddress") },
        { json: "address", js: "address", typ: r("Address") },
        { json: "attributes", js: "attributes", typ: a(r("Attribute")) },
        { json: "installments", js: "installments", typ: r("Installments") },
        { json: "winner_item_id", js: "winner_item_id", typ: null },
        { json: "discounts", js: "discounts", typ: null },
        { json: "promotions", js: "promotions", typ: a("any") },
        { json: "differential_pricing", js: "differential_pricing", typ: u(undefined, r("DifferentialPricing")) },
        { json: "inventory_id", js: "inventory_id", typ: u(null, "") },
        { json: "catalog_listing", js: "catalog_listing", typ: u(undefined, true) },
        { json: "variation_filters", js: "variation_filters", typ: u(undefined, a("")) },
        { json: "variations_data", js: "variations_data", typ: u(undefined, m(r("VariationsDatum"))) },
        { json: "official_store_name", js: "official_store_name", typ: u(undefined, "") },
    ], false),
    "Address": o([
        { json: "state_id", js: "state_id", typ: r("StateID") },
        { json: "state_name", js: "state_name", typ: r("StateName") },
        { json: "city_id", js: "city_id", typ: u(null, "") },
        { json: "city_name", js: "city_name", typ: "" },
    ], false),
    "Attribute": o([
        { json: "id", js: "id", typ: "" },
        { json: "name", js: "name", typ: "" },
        { json: "value_id", js: "value_id", typ: u(null, "") },
        { json: "value_name", js: "value_name", typ: "" },
        { json: "attribute_group_id", js: "attribute_group_id", typ: r("AttributeGroupID") },
        { json: "attribute_group_name", js: "attribute_group_name", typ: r("AttributeGroupName") },
        { json: "value_struct", js: "value_struct", typ: u(r("Struct"), null) },
        { json: "values", js: "values", typ: a(r("AttributeValue")) },
        { json: "source", js: "source", typ: 0 },
        { json: "value_type", js: "value_type", typ: r("ValueType") },
    ], false),
    "Struct": o([
        { json: "number", js: "number", typ: 3.14 },
        { json: "unit", js: "unit", typ: r("Unit") },
    ], false),
    "AttributeValue": o([
        { json: "id", js: "id", typ: u(null, "") },
        { json: "name", js: "name", typ: "" },
        { json: "struct", js: "struct", typ: u(r("Struct"), null) },
        { json: "source", js: "source", typ: 0 },
    ], false),
    "DifferentialPricing": o([
        { json: "id", js: "id", typ: 0 },
    ], false),
    "Installments": o([
        { json: "quantity", js: "quantity", typ: 0 },
        { json: "amount", js: "amount", typ: 3.14 },
        { json: "rate", js: "rate", typ: 3.14 },
        { json: "currency_id", js: "currency_id", typ: r("CurrencyID") },
    ], false),
    "Seller": o([
        { json: "id", js: "id", typ: 0 },
        { json: "nickname", js: "nickname", typ: "" },
        { json: "car_dealer", js: "car_dealer", typ: true },
        { json: "real_estate_agency", js: "real_estate_agency", typ: true },
        { json: "_", js: "_", typ: true },
        { json: "registration_date", js: "registration_date", typ: Date },
        { json: "tags", js: "tags", typ: a(r("SellerTag")) },
        { json: "car_dealer_logo", js: "car_dealer_logo", typ: "" },
        { json: "permalink", js: "permalink", typ: "" },
        { json: "seller_reputation", js: "seller_reputation", typ: r("SellerReputation") },
        { json: "eshop", js: "eshop", typ: u(undefined, r("Eshop")) },
    ], false),
    "Eshop": o([
        { json: "eshop_id", js: "eshop_id", typ: 0 },
        { json: "seller", js: "seller", typ: 0 },
        { json: "nick_name", js: "nick_name", typ: "" },
        { json: "eshop_status_id", js: "eshop_status_id", typ: 0 },
        { json: "site_id", js: "site_id", typ: r("SiteID") },
        { json: "eshop_experience", js: "eshop_experience", typ: 0 },
        { json: "eshop_rubro", js: "eshop_rubro", typ: null },
        { json: "eshop_locations", js: "eshop_locations", typ: a("any") },
        { json: "eshop_logo_url", js: "eshop_logo_url", typ: "" },
    ], false),
    "SellerReputation": o([
        { json: "level_id", js: "level_id", typ: r("LevelID") },
        { json: "power_seller_status", js: "power_seller_status", typ: u(r("PowerSellerStatus"), null) },
        { json: "transactions", js: "transactions", typ: r("Transactions") },
        { json: "metrics", js: "metrics", typ: r("Metrics") },
    ], false),
    "Metrics": o([
        { json: "sales", js: "sales", typ: r("Sales") },
        { json: "claims", js: "claims", typ: r("Cancellations") },
        { json: "delayed_handling_time", js: "delayed_handling_time", typ: r("Cancellations") },
        { json: "cancellations", js: "cancellations", typ: r("Cancellations") },
    ], false),
    "Cancellations": o([
        { json: "period", js: "period", typ: r("CancellationsPeriod") },
        { json: "rate", js: "rate", typ: 3.14 },
        { json: "value", js: "value", typ: 0 },
        { json: "excluded", js: "excluded", typ: u(undefined, r("Excluded")) },
    ], false),
    "Excluded": o([
        { json: "real_value", js: "real_value", typ: 0 },
        { json: "real_rate", js: "real_rate", typ: 3.14 },
    ], false),
    "Sales": o([
        { json: "period", js: "period", typ: r("CancellationsPeriod") },
        { json: "completed", js: "completed", typ: 0 },
    ], false),
    "Transactions": o([
        { json: "canceled", js: "canceled", typ: 0 },
        { json: "completed", js: "completed", typ: 0 },
        { json: "period", js: "period", typ: r("TransactionsPeriod") },
        { json: "ratings", js: "ratings", typ: r("Ratings") },
        { json: "total", js: "total", typ: 0 },
    ], false),
    "Ratings": o([
        { json: "negative", js: "negative", typ: 3.14 },
        { json: "neutral", js: "neutral", typ: 3.14 },
        { json: "positive", js: "positive", typ: 3.14 },
    ], false),
    "SellerAddress": o([
        { json: "comment", js: "comment", typ: "" },
        { json: "address_line", js: "address_line", typ: "" },
        { json: "id", js: "id", typ: null },
        { json: "latitude", js: "latitude", typ: null },
        { json: "longitude", js: "longitude", typ: null },
        { json: "country", js: "country", typ: r("Sort") },
        { json: "state", js: "state", typ: r("Sort") },
        { json: "city", js: "city", typ: r("Sort") },
    ], false),
    "Shipping": o([
        { json: "store_pick_up", js: "store_pick_up", typ: true },
        { json: "free_shipping", js: "free_shipping", typ: true },
        { json: "logistic_type", js: "logistic_type", typ: r("LogisticType") },
        { json: "mode", js: "mode", typ: r("Mode") },
        { json: "tags", js: "tags", typ: a(r("ShippingTag")) },
        { json: "promise", js: "promise", typ: null },
    ], false),
    "VariationsDatum": o([
        { json: "thumbnail", js: "thumbnail", typ: "" },
        { json: "ratio", js: "ratio", typ: "" },
        { json: "name", js: "name", typ: "" },
        { json: "pictures_qty", js: "pictures_qty", typ: 0 },
        { json: "inventory_id", js: "inventory_id", typ: "" },
    ], false),
    "StateID": [
        "AR-B",
        "AR-C",
        "AR-T",
        "AR-X",
    ],
    "StateName": [
        "Buenos Aires",
        "Capital Federal",
        "Córdoba",
        "Tucumán",
    ],
    "AttributeGroupID": [
        "OTHERS",
    ],
    "AttributeGroupName": [
        "Otros",
    ],
    "Unit": [
        "cm",
        "g",
        "kg",
        "m",
        "mm",
    ],
    "ValueType": [
        "list",
        "number",
        "number_unit",
        "string",
    ],
    "BuyingMode": [
        "buy_it_now",
    ],
    "CategoryID": [
        "MLA10089",
        "MLA1649",
        "MLA1672",
        "MLA1694",
        "MLA352001",
        "MLA432485",
        "MLA72894",
    ],
    "Condition": [
        "new",
    ],
    "CurrencyID": [
        "ARS",
    ],
    "DomainID": [
        "MLA-DESKTOP_COMPUTERS",
        "MLA-GAMEPADS_AND_JOYSTICKS",
        "MLA-GRAPHICS_TABLETS",
        "MLA-HARD_DRIVE_AND_SSD_CADDIES",
        "MLA-HARD_DRIVES_AND_SSDS",
        "MLA-RAM_MEMORY_MODULES",
        "MLA-STREAMING_MEDIA_DEVICES",
    ],
    "ListingTypeID": [
        "gold_pro",
        "gold_special",
    ],
    "SiteID": [
        "MLA",
    ],
    "LevelID": [
        "4_light_green",
        "5_green",
    ],
    "CancellationsPeriod": [
        "365 days",
        "60 days",
    ],
    "PowerSellerStatus": [
        "platinum",
    ],
    "TransactionsPeriod": [
        "historic",
    ],
    "SellerTag": [
        "brand",
        "credits_priority_1",
        "credits_priority_3",
        "credits_priority_4",
        "credits_profile",
        "developer",
        "eshop",
        "large_seller",
        "medium_seller",
        "messages_as_seller",
        "mshops",
        "normal",
    ],
    "LogisticType": [
        "cross_docking",
        "drop_off",
        "fulfillment",
        "xd_drop_off",
    ],
    "Mode": [
        "me2",
    ],
    "ShippingTag": [
        "fulfillment",
        "mandatory_free_shipping",
        "self_service_in",
        "self_service_out",
    ],
    "ResultTag": [
        "best_seller_candidate",
        "brand_verified",
        "cart_eligible",
        "deal_of_the_day",
        "dragged_bids_and_visits",
        "extended_warranty_eligible",
        "good_quality_picture",
        "good_quality_thumbnail",
        "immediate_payment",
        "lightning_deal",
        "loyalty_discount_eligible",
        "meli_choice_candidate",
        "moderation_penalty",
        "mshops_ahora-12",
        "mshops_ahora-6",
        "shipping_guaranteed",
        "standard_price_by_channel",
    ],
};
