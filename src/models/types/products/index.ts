export const productTypes = ()=>{
    const GET_PRODUCTS = 'GET_PRODUCTS'
    const GET_PRODUCT = 'GET_PRODUCT'
    const GET_PRODUCT_DESCRIPTION = 'GET_PRODUCT_DESCRIPTION'
    const SEARCH_PRODUCT = 'SEARCH_PRODUCT'

    return {
        GET_PRODUCT,
        GET_PRODUCTS,
        GET_PRODUCT_DESCRIPTION,
        SEARCH_PRODUCT
    }
}