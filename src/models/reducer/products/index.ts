import {ActionReducerMapBuilder, createReducer} from "@reduxjs/toolkit";
import {productTypes} from "../../types";
import {AnyAction} from "redux";
import {handleInitialStates} from "../../initialStates";

export const handleProductsReducer = () => {
    const {GET_PRODUCTS, GET_PRODUCT, GET_PRODUCT_DESCRIPTION} = productTypes()
    const {products} = handleInitialStates()
    const builderCallBack = (builder:ActionReducerMapBuilder<any>)=>{
        builder
            .addCase(GET_PRODUCTS,(state, action:AnyAction)=>{
                return {
                    ...state,
                    data:action.payload
                }
            })
            .addCase(GET_PRODUCT,(state, action:AnyAction)=>{
                return {
                    ...state,
                    item: {...state.item,...action.payload}
                }
            })
            .addCase(GET_PRODUCT_DESCRIPTION,(state, action:AnyAction)=>{
                return {
                    ...state,
                    item:{
                        ...state.item,
                        plain_text:action.payload.plain_text
                    }
                }
            })
    }
    return createReducer(products,builderCallBack)
}