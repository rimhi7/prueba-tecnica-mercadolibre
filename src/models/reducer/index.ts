import {combineReducers} from "redux";
import {handleProductsReducer} from "./products";

export const handleReducer = ()=>{
    const products = handleProductsReducer()
    return combineReducers({
        products
    })
}