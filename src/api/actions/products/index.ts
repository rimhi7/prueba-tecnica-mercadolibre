import {useProductProviders} from "../../provider";
import {Dispatch} from "react";
import {AnyAction} from "redux";
import {AxiosResponse} from "axios";
import {AxiosResult} from "../../../models/interfaces/axios";
import {Item} from "../../../models/interfaces/products";
import {productTypes} from "../../../models/types";



export const useProductActions = () => {
    const {getProductsProvider,getProductProvider, getProductDescriptionProvider, searchProductProvider} = useProductProviders()
    const {GET_PRODUCTS, GET_PRODUCT_DESCRIPTION, GET_PRODUCT} = productTypes()
    const getProductsAction = ()=> async(dispatch:Dispatch<AnyAction>) => {
        try {
            const response:AxiosResponse<AxiosResult<Item[]>> = await getProductsProvider();
            dispatch({
                type:GET_PRODUCTS,
                payload:response.data.results
            })
        }catch (e){

        }
    }

    const getProductAction = (id:string) => async (dispatch:Dispatch<AnyAction>) => {
        try {
            const response:AxiosResponse<AxiosResult<AxiosResult<Item>>> = await getProductProvider(id)
            dispatch({
                type:GET_PRODUCT,
                payload:response.data
            })
        }catch (e) {

        }
    }
    const getProductDescriptionAction = (id:string)=> async (dispatch:Dispatch<AnyAction>) => {
        try {
            const response:AxiosResponse<AxiosResult<AxiosResult<Item>>> = await getProductDescriptionProvider(id)

            dispatch({
                type:GET_PRODUCT_DESCRIPTION,
                payload:response.data
            })
        }catch (e) {

        }
    }
    const searchProductAction = (value:string)=> async (dispatch:Dispatch<AnyAction>) => {
        try {
            const response:AxiosResponse<AxiosResult<AxiosResult<Item[]>>> = await searchProductProvider(value)
            dispatch({
                type:GET_PRODUCTS,
                payload:response.data.results
            })
        }catch (e) {

        }
    }
    return {
        getProductsAction,
        getProductAction,
        getProductDescriptionAction,
        searchProductAction,
    }
}