import axios, {AxiosResponse} from "axios";
import {trackPromise} from "react-promise-tracker";
import {AxiosResult} from "../../../models/interfaces/axios";
import {Item} from "../../../models/interfaces/products";

export const useProductProviders = () => {
    const getProductsProvider = ()=> {
        const response =  axios({
            method:'GET',
            url:`/sites/MLA/search?q=pcs`
        })
        return trackPromise(response)
    }
    const getProductProvider = (id:string) => {
        const response = axios({
            method:'GET',
            url:'/items/'+id,

        })
        return trackPromise(response)
    }
    const getProductDescriptionProvider = (id:string) => {
        const response = axios({
            method:'GET',
            url:`/items/${id}/description`,
        })
        return trackPromise(response)
    }
    const searchProductProvider  = (query:string) => {
        const response = axios({
            method:'GET',
            url:`/sites/MLA/search?q=:${query}`,
        })
        return trackPromise(response)
    }
    return {
        getProductsProvider,
        getProductProvider,
        getProductDescriptionProvider,
        searchProductProvider
    }
}