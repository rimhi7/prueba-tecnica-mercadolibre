/// <reference types="cypress" />
describe('<Home/>',()=>{
  it('<Home/>  - Verificar Data',()=>{
    cy.visit('/');
    cy.contains('Pc Armada')
    cy.get('[data-cy=input-search]').should('exist')
    cy.get('[data-cy=button-search]').should('exist')
  })
  it('<Home/> - Buscar',()=>{
    cy.visit('/');
    cy.get('[data-cy=input-search]').type('One Piece')
    cy.get('[data-cy=button-search]').click()
    cy.contains('Luffy')
    cy.get('.card').first().click()
    cy.wait(1000)
    cy.go('back')
    cy.wait(5000)
    cy.get('.card').eq(2).click()
    cy.scrollTo(0,0)
    cy.wait(5000)
    cy.get('[data-cy=icon-button]').click()
    cy.wait(2500)
    cy.scrollTo(0,0)
    cy.get('.card').eq(2).click()
    cy.scrollTo(0,0)

  })
})